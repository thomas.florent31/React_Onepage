export { default as Header } from './Header/Header';
export { default as Img } from './MainOne/Img';
export { default as Text } from './MainOne/Text';
export { default as Text2 } from './MainTwo/Text';
export { default as Nav } from './Nav/Nav'
export { default as MainThree } from './MainThree/MainThree';
export { default as MainFour } from './MainFour/MainFour';
export { default as Footer } from './Footer/Footer';
