import React from 'react';
import {Nav, Header, Text, Text2, MainThree, MainFour, Footer} from './components';
function App() {
    return (
      <div className="App">
        <Nav />
        <Header />
        <Text />
        <Text2 />
        <MainThree />
        <MainFour />
        <Footer />
      </div>
    );
  }

export default App;